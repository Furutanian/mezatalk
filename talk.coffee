$ ->
	has_focus = true
	sounds = false

	org_favicon = $('#favicon').attr('href')
	org_title = $('title').text()
	blink = 0

	last_typing = new Date()
	typing_user = {}
	lfkey = if $('#lfkey4js').val() == 'Reverse' then true else false

	socket = new WebSocket($('#wsuri4js').val())

	$('html, body').animate({scrollTop: $(document).height()}, 1500)

	h2s = (hash) ->
		ss = []; for k, v of hash
			ss.push(":#{k}=>'#{v}'")
		"{#{ss.join(', ')}}"

	# 接続に成功した
	socket.onopen = ->
		hash = {}
		hash['REQUEST']	= 'login'
		hash['TYPE']	= 'talk'
		hash['USER']	= $('#user4js').val()
		hash['ROOM']	= $('#room4js').val()
		hash['DROOM']	= $('#droom4js').val()
		hash['DESC']	= $('#desc4js').val() if($('#desc4js')[0])
		socket.send(h2s(hash) + '@@login')

		# 共有メンバの追加/解除フォームを設定させる
		socket.send(h2s({ REQUEST: 'prop', TYPE: 'get' }) + '@@sharer')

		if(sound = $('#sound4js').val())
			sounds = new Sounds
			sound = sounds.set_src('NOTICE', 'sounds/' + sound)
			sound.setVolume(10)

	# 接続に失敗した
	socket.onerror = ->
		$('#messages').append("<P><SPAN class='state'>Connect error.</SPAN>")

	# 接続がクローズされた(たぶん相手から)
	socket.onclose = ->
		$('#messages').append("<P><SPAN class='state'>Unexpected closed.</SPAN>")

	# 単一行(sentence)の発言を送信する
	$('#s_send').on('click', ->
		socket.send(h2s({ REQUEST: 'sentence' }) + '@@' + $('#sentence').val())
		$('#sentence').val('')
	)
	# キータイプを報告、Return で送信
	$('#sentence').keydown((event) ->
		now = new Date()
		if(now - last_typing > 2000)
			socket.send(h2s({ REQUEST: 'typing' }) + '@@sentence')
			last_typing = now
		if(event.keyCode == 13)
			$('#s_send').click()
	)
	# 相槌を送信する
	$('#hum').on('click', ->
		socket.send(h2s({ REQUEST: 'nod' }) + '@@ふむふむ')
	)
	$('#ack').on('click', ->
		socket.send(h2s({ REQUEST: 'nod' }) + '@@そうだね')
	)
	$('#mmm').on('click', ->
		socket.send(h2s({ REQUEST: 'nod' }) + '@@ほんと？')
	)
	$('#nak').on('click', ->
		socket.send(h2s({ REQUEST: 'nod' }) + '@@いやいや')
	)

	# 複数行(paragraph)の発言を送信する
	$('#p_send').on('click', ->
		socket.send(h2s({ REQUEST: 'paragraph', CODE: $('#p_code').prop('checked') }) + '@@' + $('#paragraph').val())
		$('#p_clear').click()
	)
	# 複数行(paragraph)のコマンドを送信する
	$('#p_exec').on('click', ->
		socket.send(h2s({ REQUEST: 'exec', CODE: $('#p_code').prop('checked') }) + '@@' + $('#paragraph').val())
		$('#p_clear').click()
	)
	# キータイプを報告、Shift+Return で改行、Return で送信
	$('#paragraph').keydown((event) ->
		now = new Date()
		if(now - last_typing > 2000)
			socket.send(h2s({ REQUEST: 'typing' }) + '@@paragraph')
			last_typing = now
		if((if lfkey then !event.shiftKey else event.shiftKey) and event.keyCode == 13)
			$('#p_send').click()
			false
	)
	# ファイルがドロップされたら、アップロードする
	# https://api.jquery.com/jquery.ajax/
	$('#paragraph').on('drop', (event0) ->
		event = event0.originalEvent
		dataTransfer = event.dataTransfer
		files = dataTransfer.files
		formData = new FormData()
		n = 0; for file in files
			formData.append('upload' + n++, file)
		$.ajax({
			url:			'upload',
			type:			'post',
			data:			formData,
			contentType:	false,
			processData:	false,
		}).done((data, textStatus, jqXHR) ->	# data には upload.erb の出力である "image/xxx.jpeg\n" などが返る
			socket.send(h2s({ REQUEST: 'paragraph', MEDIA: data.trim() }) + '@@' + $('#paragraph').val())
			$('#p_clear').click()
		).fail((jqXHR, textStatus, errorThrown) ->
			alert('Upload [' + textStatus + '].')
		)
		false
	)
	$('#p_clear').on('click', ->
		$('#paragraph').val('')
		$('#p_code').prop('checked', false)
	)
	# 発言の修正のため、発言を複数行(paragraph)欄に呼び戻す
	$('#messages').on('click', '.seq', ->
		seq = $(this).text()
		$('#paragraph').val('update ' + seq + '\n' + $('#t' + seq).text())
		$('#p_code').prop('checked', if $('#t' + seq).attr('class') == 'code' then true else false)
	)
	# アイコン(＋返答)を付与する
	$('#icons').on('click', '[id^=icon_]', ->
		socket.send(h2s({ REQUEST: 'icon', ICON: $(@).html(), SEQ: $('#paragraph').val() }) + '@@' + $('#sentence').val())
		$('#sentence').val('')
		$('#p_clear').click()
	)

	# メッセージを受信した
	socket.onmessage = (event) ->
		[command, body...] = event.data.split(':')
		# 発言
		if(command == 'message')
			user_id = body.shift()
			typing_user[user_id] = 0; typing_end(user_id)
			$('#messages').append(body.join(':'))
			unless(has_focus)
				sounds and sounds.se('NOTICE').out()
				$('title').text('♬' + org_title)
				$('#favicon').remove()
				$('head').append("<LINK rel='icon' id='favicon' href='favicon_n.png' type='image/png'>")
#				$('#favicon').attr('href', 'favicon_n.png')
				blink = 1
		# 発言の修正
		else if(command == 'update')
			seq = body.shift()
			$('#p' + seq).html(body.join(':'))
		# キーがタイプされた
		else if(command == 'typing')
			user_id = body.shift()
			typing_user[user_id] ||= 0; typing_user[user_id]++
			$('#status').append(body.join(':')) if(typing_user[user_id] == 1)
			setTimeout(typing_end, 3000, user_id)
		# 相槌を打った
		else if(command == 'nod')
			user_id = body.shift()
			$('#status').append(body.join(':'))
			setTimeout(nod_end, 3000, user_id)
		# 共有メンバの追加/解除フォームを設定する
		else if(command == 'prop')
			key = body.shift()
			if(key == 'sharer')
				sharers = $('#sharer').find('INPUT')
				for sharer in sharers
					$(sharer).prop('checked', if body.includes($(sharer).attr('value')) then true else false)
		# ダミー応答
		else if(command == 'mark')
			# 何もしない
		else
			$('#messages').append("<P><SPAN class='state'>Unexpected message received.</SPAN>")
			console.log('Unexpected. %o', event)

	# キーがタイプされた旨の表示を消す
	typing_end = (user_id) ->
		typing_user[user_id]-- if(typing_user[user_id] > 0)
		$('#t' + user_id).remove() if(typing_user[user_id] < 1)
	# 相槌を打った旨の表示を消す
	nod_end = (user_id) ->
		$('#t' + user_id).remove()

	# 共有メンバの追加/解除フォームを出し入れする
	$('#r_toggle').on('click', ->
		$('#sharer').toggle('slow')
	)
	# 共有メンバを追加/解除する
	$('#r_send').on('click', ->
		sharers = $('#sharer').find('INPUT')
		$(sharers).fadeOut('fast')
		body = []; for sharer in sharers
			body.push($(sharer).attr('value')) if($(sharer).prop('checked'))
		socket.send(h2s({ REQUEST: 'prop', TYPE: 'set' }) + '@@sharer:' + body.join(':'))
		$(sharers).fadeIn('fast')
	)
	$('#r_reset').on('click', ->
		sharers = $('#sharer').find('INPUT')
		for sharer in sharers
			$(sharer).prop('checked', false)
	)
	# スケジュールを回答
	$('#messages').on('click', ':button[id=sched_ans]', ->
		schedules = $('#' + (id = $(@).attr('value'))).find('INPUT')
		body = []; body.push(id)
		for schedule in schedules
			if($(schedule).prop('type') == 'hidden')
				body.push($(schedule).prop('value'))
			if($(schedule).prop('type') == 'checkbox')
				body.push($(schedule).prop('checked'))
		body.push('p' + $('#paragraph').val())
		socket.send(h2s({ REQUEST: 'schedule' }) + '@@' + body.join(':'))
		$('#p_clear').click()
	)
	# スケジュールのチェックボックスをまとめてON/OFF
	$('#messages').on('click', '[id^=sched]', ->
		checkboxes = $('#' + $(@).attr('id') + 'c').children('INPUT')
		check = if $(checkboxes[1]).prop('checked') then false else true
		for checkbox in checkboxes
			$(checkbox).prop('checked', if $(checkbox).prop('disabled') then false else check)
	)
	# スケジュールを集計
	$('#messages').on('click', ':button[id=sched_res]', ->
		socket.send(h2s({ REQUEST: 'exec' }) + '@@sched result ' + $(@).attr('value'))
	)

	# 新着の発言を点滅して知らせる
	do_blink = ->
		if(blink)
			$('title').text((if blink % 3 != 0 then '♬' else '') + org_title)
			blink++
		else
			$('title').text(org_title)

	if(blink = $('#blink4js').val())
		setInterval(do_blink, blink)

	# ウィンドウにフォーカスが当たった
	$(window).on('focus', ->
#		console.log('window focus in.')
		has_focus = true
		$('title').text(org_title)
		$('#favicon').remove()
		$('head').append("<LINK rel='icon' id='favicon' href='#{org_favicon}' type='image/png'>")
#		$('#favicon').attr('href', org_favicon)	# 旧い Opera だと戻らず
		blink = 0

	# ウィンドウからフォーカスが外れた
	).on('blur', ->
#		console.log('window focus out.')
		has_focus = false
	)

	0

