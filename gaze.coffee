$ ->
	has_focus = true
	sounds = false

	org_favicon = $('#favicon').attr('href')
	org_title = $('title').text()
	blink = 0

	socket = new WebSocket($('#wsuri4js').val())
	user = $('#user4js').val()
	ucrt = $('#ucrt4js').val()

	h2s = (hash) ->
		ss = []; for k, v of hash
			ss.push(":#{k}=>'#{v}'")
		"{#{ss.join(', ')}}"

	# 接続に成功した
	socket.onopen = ->
		hash = {}
		hash['REQUEST']	= 'login'
		hash['TYPE']	= 'gaze'
		hash['USER']	= user
		socket.send(h2s(hash) + '@@login')

		if(sound = $('#sound4js').val())
			sounds = new Sounds
			sound = sounds.set_src('NOTICE', 'sounds/' + sound)
			sound.setVolume(10)

	# 接続に失敗した
	socket.onerror = ->
		$('#status').append("<P><SPAN class='state'>Connect error.</SPAN>")

	# 接続がクローズされた(たぶん相手から)
	socket.onclose = ->
		$('#status').append("<P><SPAN class='state'>Unexpected closed.</SPAN>")

	# 更新されたルームの表示数を増やす
	$('#more_update_rooms').on('click', ->
		entries = $('#update_rooms').children('SPAN')
		socket.send(h2s({ REQUEST: 'moreupdate', ENTRIES: entries.length }) + '@@')
	)

	# 関与している更新されたルームの表示数を増やす
	$('#more_sharer_update_rooms').on('click', ->
		entries = $('#sharer_update_rooms').children('SPAN')
		socket.send(h2s({ REQUEST: 'moresharerupdate', ENTRIES: entries.length }) + '@@')
	)

	# メッセージを受信した
	socket.onmessage = (event) ->
		[command, body...] = event.data.split(':')
		quserucrt = 'user=' + user + ';ucrt=' + ucrt + ';'
		# 全員向けの更新が通知された
		if(command == 'update_room')
			id = body.shift()
			rooms = $("#update_rooms").children('SPAN')
			for room in rooms
				break if(id == $(room).attr('id'))
			$(room).fadeOut('slow').queue( ->
				@.remove()
				$('#update_rooms').prepend(body.join(':').replace(/user=%s;ucrt=%s;/, quserucrt))
			)
		# 共有メンバのみ向けの更新が通知された
		else if(command == 'sharer_update_room')
			id = body.shift()
			rooms = $("#sharer_update_rooms").children('SPAN')
			for room in rooms
				break if(id == $(room).attr('id'))
			$(room).fadeOut('slow').queue( ->
				@.remove()
				$('#sharer_update_rooms').prepend(body.join(':').replace(/user=%s;ucrt=%s;/, quserucrt))
			)
			unless(has_focus)
				sounds and sounds.se('NOTICE').out()
				$('title').text('♬' + org_title)
				$('#favicon').remove()
				$('head').append("<LINK rel='icon' id='favicon' href='favicon_n.png' type='image/png'>")
#				$('#favicon').attr('href', 'favicon_n.png')
				blink = 1
		# 全員向けの更新の追加が通知された
		else if(command == 'update_room_plus')
			id = body.shift()
			$('#update_rooms').append(body.join(':').replace(/user=%s;ucrt=%s;/, quserucrt))

		# 共有メンバのみ向けの更新の追加が通知された
		else if(command == 'sharer_update_room_plus')
			id = body.shift()
			$('#sharer_update_rooms').append(body.join(':').replace(/user=%s;ucrt=%s;/, quserucrt))

		# ダミー応答
		else if(command == 'mark')
			# 何もしない
		else
			$('#status').append("<P><SPAN class='state'>Unexpected message received.</SPAN>")
			console.log('Unexpected. %o', event)

	# 新着の発言を点滅して知らせる
	do_blink = ->
		if(blink)
			$('title').text((if blink % 3 != 0 then '♪' else '') + org_title)
			blink++
		else
			$('title').text(org_title)

	if(blink = $('#blink4js').val())
		setInterval(do_blink, blink)

	# 任意の場所がクリックされた
	$(document).click( ->
		blink = 0
	)

	# ウィンドウにフォーカスが当たった
	$(window).on('focus', ->
#		console.log('window focus in.')
		has_focus = true
		$('title').text(org_title)
		$('#favicon').remove()
		$('head').append("<LINK rel='icon' id='favicon' href='#{org_favicon}' type='image/png'>")
#		$('#favicon').attr('href', org_favicon)	# 旧い Opera だと戻らず
		blink = 0

	# ウィンドウからフォーカスが外れた
	).on('blur', ->
#		console.log('window focus out.')
		has_focus = false
	)

	0

