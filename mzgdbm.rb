# coding: utf-8

#-------------------------------------------------------------------------------
#
#	連番処理モジュール
#
module SeqHash

	@@start = 100
	@@last = '#last#'

	def initialize(*params)
		super
		self[@@last] ||= @@start.to_s
		self.respond_to?(:regist_internal_keys) and regist_internal_keys(@@last)
	end

	#-----------------------------------------------------------
	#
	#	最後の連番を返す
	#
	def last_seq
		self[@@last]
	end

	#-----------------------------------------------------------
	#
	#	加算した連番を返す
	#
	def inc_seq
		self[@@last] = (self[@@last].to_i + 1).to_s
	end

	#-----------------------------------------------------------
	#
	#	連番管理の要素を追加する
	#
	def <<(value)
		self[s_seq = inc_seq] = value
		s_seq
	end
end

#-------------------------------------------------------------------------------
#
#	ファイル分散配置処理モジュール
#
require 'digest/md5'

module ClusterFile

	def initialize(*params)
		paths = params[0].split('/')
		fname = paths.pop
		paths << '_%s' % Digest::MD5.hexdigest(fname)[0, 2]
		File.exist?(it = paths.join('/')) or Dir.mkdir(it)
		params[0] = (paths << fname).join('/')
		super
	end
end

#===============================================================================
#
#	連番処理/ファイル分散配置処理付き GDBM クラス
#
require 'gdbm'

class ClusterSeqGDBM < GDBM

	include ClusterFile
	include SeqHash

	def self.open(*params)
		begin
			db = self.new(*params)
		rescue(Errno::ENOENT)
			db = false							# DB ファイルが存在しなければ false を返す
		end
		if(db and block_given?)
			yield(db)
			db.close
		else
			db
		end
	end

	def initialize(*params)
		@internal_keys = []
		super
	end

	def regist_internal_keys(*keys)
		@internal_keys += keys
	end

	alias :org_size :size
	def size
		org_size - @internal_keys.size
	end

#	def empty?
#		self.size == 0
#	end
end

#===============================================================================
#
#	Mezatalk 発言管理用 GDBM クラス
#
require 'cgi'

class MzGDBM < ClusterSeqGDBM

	#-----------------------------------------------------------
	#
	#	メッセージを内部形式で DB に追加登録する
	#
	def store_message(ip, user, css_class, text)
		update_message(inc_seq, ip, user, css_class, text)
	end

	#-----------------------------------------------------------
	#
	#	メッセージを内部形式で DB に更新登録する
	#
	def update_message(s_seq, ip, user, css_class, text)
		it = self[s_seq] and (s_seqx = s_seq; 6.times { self[s_seqx += 'x'] or break }; self[s_seqx] = it)	# 更新履歴を残す
		self[s_seq] = '%d:%s:%s:%s:%s' % [Time.now.to_i, ip, user, css_class, text]
		s_seq
	end

	#-----------------------------------------------------------
	#
	#	メッセージを内部形式で DB に追加登録し、HTML 形式で返す
	#
	def store_message_plus(ip, user, css_class, text)
		Time.to_h_clear(Time.at((self[last_seq] || '0:').split(':', 2)[0].to_i))
		s_seq = store_message(ip, user, css_class, text)
		_tagged_message(s_seq)
	end

	#-----------------------------------------------------------
	#
	#	メッセージを内部形式で DB に更新登録し、HTML 形式で返す
	#
	def update_message_plus(s_seq, ip, user, css_class, text)
		Time.to_h_clear(Time.at((self[(s_seq.to_i - 1).to_s] || '0:').split(':', 2)[0].to_i))
		s_seq = update_message(s_seq, ip, user, css_class, text)
		_tagged_message(s_seq)
	end

	#-----------------------------------------------------------
	#
	#	アイコン(＋返答)を内部形式で DB に更新登録する
	#
	def attach_icon(s_seq, ip, user, icon, text)
		s_seq_n = nil; 99.times {|n|
			self[it = '%s_%d' % [s_seq, n + 1]] or (s_seq_n = it and break)
		}
		s_seq_n ||= 99
		self[s_seq_n] = '%d:%s:%s:%s:%s' % [Time.now.to_i, ip, user, icon, text]
		s_seq
	end

	#-----------------------------------------------------------
	#
	#	アイコン(＋返答)を内部形式で DB に更新登録し、HTML 形式で返す
	#
	def attach_icon_plus(s_seq, ip, user, icon, text)
		s_seq = attach_icon(s_seq, ip, user, icon, text)
		_tagged_message(s_seq)
	end

	#-----------------------------------------------------------
	#
	#	内部形式のメッセージを HTML 形式の配列で返す
	#
	def _tagged_message(s_seq)
		time, ip, user, css_class, text = self[s_seq].force_encoding('UTF-8').split(':', 5)
		h_time = Time.at(time.to_i).to_h
		h_tsep = h_time.size > 5 ? "<HR width='40%'>\n" : ''
		correct = self[s_seq + 'x'] ?
			"<SPAN class='correct'>訂</SPAN>" : ''
		if(css_class == 'html')
			tagged_text = text
			css_class = 'text'
		elsif(css_class == 'deleted')
			return([])
		else
			uris = []; n = 0; text.gsub!(/https?:\/\/[\x21-\x3b\x3d\x3f-\x7e]+/) {|uri|
				uris << [d_uri = (n += 1).to_s + Digest::MD5.hexdigest(uri), uri]
				d_uri
			}
			tagged_text = CGI.escapeHTML(text)
			uris.each {|d_uri, uri|
				tagged_text.gsub!(d_uri, "<A href='%s'>%s</A>" % [uri, uri])
			}
		end
		lines = tagged_text.split(/\n/)

		# アップロードファイルが含まれる場合										# css_class: 'image/x/.image1.jpg//image/y/.image2.jpg'
		uploads = []; css_class.split('//').each {|medium|							#	 medium: 'image/x/.image1.jpg'
			if((ms = medium.split('/')).size > 1)									#		 ms: ['image', 'x', '.image1.jpg']
				ms1 = ms[1..-1].join('/')											#		ms1: 'x/.image1.jpg'
				case(ms[0])
				  when('image')
					uploads << "<IMG src='uploads/%s'>" % ms1
					ms[-1] =~ /^\.(.+)/ and uploads[-1] = "<A href='uploads/%s' target='_blank'>%s</A>" % [(ms[-1] = $1; ms[1..-1].join('/')), uploads[-1]]
				  when('audio')
					uploads << "<AUDIO src='uploads/%s' controls></AUDIO>" % ms1
				  when('video')
					uploads << "<VIDEO src='uploads/%s' height='128' controls></VIDEO>" % ms1
				  else
					uploads << "<A href='uploads/%s' target='_blank'>Download[%s]</A>" % [ms1, CGI.escapeHTML(ms[-1])]
				end
			end
		}
		uploads.size > 0 and lines.unshift(uploads.join(' ')) and css_class = 'caption'

		# アイコン(＋返答)を付与	TODO: 時間表示が狂う
		icons = [[], []]; ihtml = ['', '']; 99.times {|n|
			it = self[s_seq_n = '%s_%d' % [s_seq, n + 1]] or break
			itime, ip, iuser, icon, text = it.force_encoding('UTF-8').split(':', 5)
			icons[text.size == 0 ? 0 : 1] << ("<SPAN class='icon' title='%s +%s'>%s%s</SPAN><SPAN class='user'>(%s)</SPAN>" % [s_seq_n, (itime.to_i - time.to_i).to_dth, icon, text, iuser])
		}
		icons[0].size > 0 and ihtml[0] = "\n" + icons[0].join("\n")
		icons[1].size > 0 and ihtml[1] = "<BR>\n" + icons[1].join("<BR>\n")

		[	"%s<P class='message' id='p%s'>"			% [h_tsep, s_seq],
			"<SPAN class='seq'>%s</SPAN>"				% [s_seq],
			"<SPAN class='user'>%s(%s)</SPAN>%s%s<BR>"	% [CGI.escapeHTML(user), h_time, correct, ihtml[0]],
			"<SPAN class='%s' id='t%s'\n>%s</SPAN>%s\n"	% [css_class, s_seq, lines.join(css_class == 'code' ? "\n" : "<BR>\n"), ihtml[1]] ]
	end

	#-----------------------------------------------------------
	#
	#	指定の番号のメッセージを HTML 形式で返す
	#
	def get_tagged_message(seq)
		_tagged_message(seq.to_s)
	end

	#-----------------------------------------------------------
	#
	#	更新されたルームに表示する属性を返す
	#
	def pick_update_head_props(update_head_props)
		props = {}; (it = update_head_props) and it.each {|key|
			(it = self['#%s#' % key]) and props[key] = it.force_encoding('UTF-8')
		}
		props
	end

	#-----------------------------------------------------------
	#
	#	リマインド登録
	#
	def store_remind(r_time, t_time, o_time, ip, user, room, text)
		s_seq = self << 'REMIND:%s:%s:%d:%s:%s:%s' % [r_time, t_time, o_time, user, room, text]
		offset = o_time == 0 ? '' : ' %d分%s' % [o_time.abs, o_time < 0 ? '前' : '過']
		"【リマインド[%s] 登録 %s%s】\n" % [r_time[6, 2] + s_seq, t_time.ngtime_to_h, offset]
	end

	#-----------------------------------------------------------
	#
	#	チャイム登録
	#
	def store_chime(w_time, r_time, t_time, o_time, ip, user, room, text)
		s_seq = self << 'CHIME:%s:%s:%s:%d:%s:%s:%s' % [w_time, r_time, t_time, o_time, user, room, text]
		offset = o_time == 0 ? '' : ' %d分%s' % [o_time.abs, o_time < 0 ? '前' : '過']
		"【チャイム[%s] 登録 %s %s%s】\n" % [r_time[0, 2] + s_seq, w_time.ngwday_to_h, t_time.ngtime_to_h(4), offset]
	end

	#-----------------------------------------------------------
	#
	#	既存のリマインド情報を返す
	#
	def get_reminds(target_room)	# TODO: 過ぎているものは表示しない
		res = []; self.each {|s_seq, v|
			if(v =~ /^REMIND:/)
				head, r_time, t_time, o_time0, user, room, text = v.force_encoding('UTF-8').split(':', 7)
				if(target_room == room)
					o_time = o_time0.to_i; offset = o_time == 0 ? '' : ' %d分%s' % [o_time.abs, o_time < 0 ? '前' : '過']
					res << [r_time, "【リマインド[%%s%s] %s%s】%s - %s" % [s_seq, t_time.ngtime_to_h, offset, CGI.escapeHTML(text), CGI.escapeHTML(user)]]
					block_given? and yield(res.last)
				end
			end
		}
		res
	end

	#-----------------------------------------------------------
	#
	#	既存のチャイム情報を返す
	#
	def get_chimes(target_room)
		res = []; self.each {|s_seq, v|
			if(v =~ /^CHIME:/)
				head, w_time, r_time, t_time, o_time0, user, room, text = v.force_encoding('UTF-8').split(':', 8)
				if(target_room == room)
					o_time = o_time0.to_i; offset = o_time == 0 ? '' : ' %d分%s' % [o_time.abs, o_time < 0 ? '前' : '過']
					res << [r_time, "【チャイム[%%s%s] %s%s%s】%s - %s" % [s_seq, w_time.ngwday_to_h, t_time.ngtime_to_h(4), offset, CGI.escapeHTML(text), CGI.escapeHTML(user)]]
					block_given? and yield(res.last)
				end
			end
		}
		res
	end

	#-----------------------------------------------------------
	#
	#	既存のリマインド情報を削除する
	#
	def delete_remind(target_room, s_seq)
		it = self[s_seq] and (head, r_time, t_time, o_time0, user, room, text = it.force_encoding('UTF-8').split(':', 7))
		if(head == 'REMIND' and target_room == room)
			self.delete(s_seq)
			o_time = o_time0.to_i; offset = o_time == 0 ? '' : ' %d分%s' % [o_time.abs, o_time < 0 ? '前' : '過']
			"【リマインド[%%s%s] %s%s】%s - %s" % [s_seq, t_time.ngtime_to_h, offset, CGI.escapeHTML(text), CGI.escapeHTML(user)]
		else
			"Remind not found."
		end
	end

	#-----------------------------------------------------------
	#
	#	既存のチャイム情報を削除する
	#
	def delete_chime(target_room, s_seq)
		it = self[s_seq] and (head, w_time, r_time, t_time, o_time0, user, room, text = it.force_encoding('UTF-8').split(':', 8))
		if(head == 'CHIME' and target_room == room)
			self.delete(s_seq)
			o_time = o_time0.to_i; offset = o_time == 0 ? '' : ' %d分%s' % [o_time.abs, o_time < 0 ? '前' : '過']
			"【チャイム[%%s%s] %s%s%s】%s - %s" % [s_seq, w_time.ngwday_to_h, t_time.ngtime_to_h(4), offset, CGI.escapeHTML(text), CGI.escapeHTML(user)]
		else
			"Chime not found."
		end
	end
end

#===============================================================================
#
#	Mezatalk 更新管理用 GDBM クラス
#
class MzHeadGDBM < ClusterSeqGDBM

	def get_tagged_update_room(id, room, props = nil, s_seq = nil, tagging_proc = nil)
		a_name = s_seq ? '#%s' % s_seq : ''
		d_room = room.split('~', 2)[1]
		if(it = tagging_proc)
			props and props = eval(props)
			it.call(id, CGI.escape(room), a_name, d_room, props)
		else
			"<SPAN id='%s'\n>・<A href='talk?user=%%s;ucrt=%%s;room=%s%s' target='_blank'>%s</A><BR></SPAN>" % [id, CGI.escape(room), a_name, d_room]
		end
	end

	#-----------------------------------------------------------
	#
	#	更新されたルームを順に HTML 形式で返す
	#
	def get_tagged_update_rooms(list_max: 10, scan_max: 200, id_prefix: '', wo_regex: nil, tagging_proc: nil)
		res = []; s_seq = self.last_seq; dup = {}
		scan_max.times {
			if(it = self[s_seq] and (room, props = it.force_encoding('UTF-8').split(':', 2)) and !dup[room]) # URI decoded
				dup[room] = true
				wo_regex and wo_regex =~ room and next
				res << get_tagged_update_room(id_prefix + CGI.escape(room), room, props, s_seq, tagging_proc)
				(list_max -= 1) < 1 and break
			end
			s_seq = (s_seq.to_i - 1).to_s
		}
		res
	end
end

#===============================================================================
#
#	Mezatalk ユーザ設定用 GDBM クラス
#
class MzUserDB

	def initialize(dbname)
		@configs = {							# デフォルト値
			:THEME	=> 'brdk',
			:LFKEY	=> 'Reverse',
			:SOUND	=> 'OFF',
			:VOLUME	=> '0',
			:BLINK	=> '0',
			:ADHEAD	=> '',
			:RPLMSG	=> '',
		}
		ClusterSeqGDBM.open(@dbname = dbname, 0666, GDBM::NOLOCK | GDBM::READER) {|db|
			@configs.keys.each {|sym|
				(it = db[sym.to_s]) and @configs[sym] = it
			}
		}										# GDBM の挙動とは異なり、すぐ閉じる
	end

	def theme
		@configs[:THEME]
	end

	def theme=(theme)
		set(:THEME, theme)
	end

	def lfkey
		@configs[:LFKEY]
	end

	def lfkey=(lfkey)
		set(:LFKEY, lfkey)
	end

	def sound
		(it = @configs[:SOUND]) != 'OFF' ? it : false
	end

	def sound=(sound)
		set(:SOUND, sound)
		sound == 'OFF' and set(:VOLUME, '0')
	end

	def volume
		@configs[:VOLUME].to_i
	end

	def volume=(volume)
		set(:VOLUME, volume.to_s)
	end

	def blink
		(it = @configs[:BLINK].to_i) != 0 ? it : false
	end

	def blink=(blink)
		set(:BLINK, blink.to_s)
	end

	def adhead
		@configs[:ADHEAD]
	end

	def adhead=(adhead)
		set(:ADHEAD, adhead)
	end

	def rplmsg
		@configs[:RPLMSG]
	end

	def rplmsg=(rplmsg)
		set(:RPLMSG, rplmsg)
	end

	def set(sym, value)
		ClusterSeqGDBM.open(@dbname) {|db|
			db[sym.to_s] = @configs[sym] = value
		}
	end
end

#===============================================================================
#
#	人間が知覚しやすい時刻表示
#
class Time

	@@to_h_last = Time.at(0)

	def self.to_h_clear(time = Time.at(0))
		@@to_h_last = time
	end

	def to_h
		format = ['%Y/', '%m/', '%d ', '(%a) ', '%H:', '%M']

		lasts = @@to_h_last.strftime(format.join).split(/[\/():]/)
		times = (@@to_h_last = self).strftime(format.join).split(/[\/():]/)

		lasts.each_index {|n|
			lasts[n] == times[n] ? format.shift : break
			format.size == 2 and break
		}
		self.strftime(format.join)
	end

	def self.to_h(time = Time.now)
		time.to_h
	end
end

#===============================================================================
#
#	人間が知覚しやすい時刻差表示
#
class Integer

	def to_dth
		self < 60 and return('%ds' % self)
		self < 3600 and return('%dm' % (self / 60))
		self < 86400 and return('%dh' % (self / 3600))
		'%dd' % (self / 86400)
	end
end

#===============================================================================
#
#	連続する数字の時刻表示を変換
#
#	'202102172211' -> '2021-02-17 22:11' / 2021-02-17 22:11:00 +0900
#
class String

	def ngtime_to_h(st = 0)
		ts = self.scan(/.{2}/)
		['%s', '%s', '-%s', '-%s', ' %s', ':%s', ':%s', '.%s', '%s'][st, ts.size].join % ts
	end

	def ngtime_to_t
		Time.local(*(self.ngtime_to_h.split(/[- :]/)))
	end

	@@jwdays = %w(日 月 火 水 木 金 土 祝)
	def ngwday_to_h
		res = ''; self.each_char {|w|
			res << @@jwdays[w.to_i]				# TODO: 英字は日曜になる
		}
		res
	end
end

__END__

