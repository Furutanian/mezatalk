FROM fedora:32

LABEL maintainer="Furutanian <furutanian@gmail.com>"

ARG http_proxy
ARG https_proxy

RUN set -x \
	&& dnf install -y \
		ruby \
		rubygem-bundler	\
		ImageMagick \
		ImageMagick-devel \
		procps-ng \
		net-tools \
		diffutils \
	&& rm -rf /var/cache/dnf/* \
	&& dnf clean all
RUN set -x \
	&& dnf install -y \
		ruby-devel \
		redhat-rpm-config \
		gcc \
		gcc-c++ \
		make \
		zlib-devel \
		patch \
		coffee-script \
	&& rm -rf /var/cache/dnf/* \
	&& dnf clean all

# git clone mezatalk しておくこと
ARG TARGET=mezatalk
RUN set -x \
	&& /usr/sbin/useradd user \
	&& /usr/bin/mkdir /home/user/${TARGET}
COPY ${TARGET}/Gemfile* /home/user/${TARGET}/
RUN set -x \
	&& /usr/bin/chown -R user:user /home/user
USER user
WORKDIR /home/user/${TARGET}
RUN set -x \
	&& bundle config set path ../bundle \
	&& bundle install \
	&& bundle clean -V

USER root
COPY ${TARGET} /home/user/${TARGET}
RUN set -x \
	&& /usr/bin/coffee -b -c *.coffee \
	&& /usr/bin/curl http://code.jquery.com/jquery-2.1.3.min.js > public/jquery.js \
	&& /usr/bin/chown -R user:user /home/user/${TARGET}

EXPOSE 8080
EXPOSE 33109

# Dockerfile 中の設定スクリプトを抽出するスクリプトを出力、実行
COPY Dockerfile .
RUN echo $'\
cat Dockerfile | sed -n \'/^##__BEGIN0/,/^##__END0/p\' | sed \'s/^#//\' > startup.sh\n\
' > extract.sh && bash extract.sh

# docker-compose up の最後に実行される設定スクリプト
##__BEGIN0__startup.sh__
#
#	if [ ! -e pv/mezatalk.config ]; then
#		cp mezatalk.config.sample pv
#		echo 'Rename 'pv/mezatalk.config.sample' to 'pv/mezatalk.config' and modify it.'
#		tail -f /dev/null
#	fi
#
#	mkdir -p pv/uploads
#
#	s=S
#	while true; do
#		pgrep -f mezatalk_wss > /dev/null
#		if [ $? -ne 0 ]; then
#			echo "`date`: ${s}tart mezatalk_wss."
#			bundle exec mezatalk_wss &
#		fi
#		pgrep -f puma > /dev/null
#		if [ $? -ne 0 ]; then
#			echo "`date`: ${s}tart puma."
#			bundle exec puma -e production --pidfile /tmp/rack.pid -b tcp://0.0.0.0:8080 &
#		fi
#		pgrep -f wsclient_remind > /dev/null
#		if [ $? -ne 0 ]; then
#			echo "`date`: ${s}tart wsclient_remind."
#			bundle exec wsclient_remind &
#		fi
#		s=Res
#		sleep 5
#	done
#
##__END0__startup.sh__

USER user
ENTRYPOINT ["bash", "startup.sh"]

