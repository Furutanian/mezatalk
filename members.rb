# coding: utf-8

=begin

* members.rb

	メンバ一覧

=end

require 'net/ldap'

class Members

	attr_reader :status

	def initialize(configs)
		@configs = configs
		@members = []
		@name2uid = {}
		@status = false
	end

	def load
		begin
			ldap = Net::LDAP.new(
				host:	@configs[:ldap_host],
				port:	@configs[:ldap_port],
			)
			results = ldap.search(
				filter:		'(listOrder=*)',
				base:		@configs[:ldap_search_base],
			)
			results.sort {|key_a, key_b|
				-(key_a['listOrder'][0].to_i <=> key_b['listOrder'][0].to_i)
			}.each {|result|
				@members << {
					:UID	=> (u = result['uid'][0]),				# ユーザ名(数値ではない)
					:LORDER	=>		result['listOrder'][0].to_i,
					:MAVEID	=> (n = result['maveid'][0].force_encoding('UTF-8')),
					:MAIL	=>		result['mail'][0],
				}
				@name2uid[n] = u
			}
			@status = true
		rescue
		end if(@configs[:ldap_host])

		lorder = 900; @names = [
			'金剛',
			'比叡',
			'榛名',
			'霧島',
			'扶桑',
			'山城',
			'伊勢',
			'日向',
			'長門',
			'陸奥',
			'大和',
			'武蔵',
		].each {|name|
			@members << {
				:UID	=> (u = name),
				:LORDER	=>		lorder += 1,
				:MAVEID	=> (n = name),
				:MAIL	=>		'',
			}
			@name2uid[n] = u
		} if(size < 1)
	end

	def size
		@members.size
	end

	def each(min = 0)
		@members.each {|member|
			yield(member) if(member[:LORDER] > min)
		}
	end

	def each_uid(min = 0)
		each(min) {|member|
			yield(member[:UID])
		}
	end

	def each_name(min = 0)
		each(min) {|member|
			yield(member[:MAVEID])
		}
	end

	def names(min = 0)
		names = []; each(min) {|member|
			names << member[:MAVEID]
		}
		names
	end

	def name2uid(name)
		@name2uid[name]
	end
end

__END__

