# coding: utf-8

require 'cgi'

class CompatCGI

	attr_reader :params

	def initialize(request)

		if(request.env['REQUEST_METHOD'] =~ /GET/i)
			@params = Rack::Utils.parse_query(request.query_string)

		elsif(request.env['REQUEST_METHOD'] =~ /POST/i)
			unless(it = request.env['CONTENT_TYPE'] and (it.split(/;\s*/))[0] =~ /multipart/)
				request.body.rewind
				@params = Rack::Utils.parse_query(request.body.read)
			else
				require 'rack/multipart'
				@params = Rack::Multipart.parse_multipart(request.env)
				@params.each {|k, v|
					v.is_a?(String) and @params[k] = CGI.unescape(v)
				}
			end
		end

		def @params.[](key)
			value = super(key)
			value.is_a?(Array) and return(value)
			value.is_a?(Hash) and return(value)
			value.is_a?(String) and return([value])
			value.is_a?(NilClass) and return([])
			raise('Unexpected error.')
		end
	end
end

__END__

