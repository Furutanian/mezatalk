# http://sinatrarb.com/intro-ja.html

require 'sinatra'

get('/gaze') {
	erb :gaze
}

get('/talk') {
	erb :talk
}

get('/setting') {
	erb :setting
}

post('/upload') {
	erb :upload
}

get('/*') {
	'?'
}

__END__

